document.onreadystatechange = function() {
    if (document.readyState == 'complete') {
        var btnHideShowOptions = document.getElementById('btnHideShowOptions');
        btnHideShowOptions.onclick = btnHideOptions_click;
        renderList();
    }
}

function btnHideOptions_click() {
    var btnHideShowOptions = document.getElementById('btnHideShowOptions');
    var options = document.getElementById('options');
    options.classList.add("hide");
    options.classList.remove("show");
    btnHideShowOptions.textContent = "Afficher";
    btnHideShowOptions.onclick = btnShowOptions_click;
}

function btnShowOptions_click() {
    var btnHideShowOptions = document.getElementById('btnHideShowOptions');
    var options = document.getElementById('options');
    options.classList.add("show");
    options.classList.remove("hide");
    btnHideShowOptions.textContent = "Masquer";
    btnHideShowOptions.onclick = btnHideOptions_click;
}

function getItems() {
    return [
        {ref:'0123', name:'Article1', description:'Description Article 1', price:'100'},
        {ref:'4567', name:'Article2', description:'Description Article 2', price:'150'},
        {ref:'8910', name:'Article3', description:'Description Article 3', price:'200'}
    ];
}

function renderList() {
    console.log("> btnList_click()")
    var listZone = document.getElementById('listZone');
    var items = getItems();
    items.forEach(item => {
        console.log(item);
        var itemNode = document.createElement("div");
        itemNode.classList.add("item");

        // définition de [ref]
        var ref = document.createElement("div");
        ref.classList.add("ref");
        var textNode = document.createTextNode(item.ref);
        ref.appendChild(textNode);
        itemNode.appendChild(ref);

        // définition de [name]
        var name = document.createElement("div");
        name.classList.add("name");
        textNode = document.createTextNode(item.name);
        name.appendChild(textNode);
        itemNode.appendChild(name);

        // définition de [description]
        var description = document.createElement("div");
        description.classList.add("description");
        textNode = document.createTextNode(item.description);
        description.appendChild(textNode);
        itemNode.appendChild(description);

        // définition de [price]
        var price = document.createElement("div");
        price.classList.add("price");
        textNode = document.createTextNode(item.price);
        price.appendChild(textNode);
        itemNode.appendChild(price);

        listZone.appendChild(itemNode);
    });
}